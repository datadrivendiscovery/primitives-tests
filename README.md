# Primitive tests

This repository is designed to test all primitives behavior's in the D3M environment 
as they are submitted to the library. It samples primitive hyperparameter values as 
well as tests primitives in basic pipeline setups. 

First the pipeline is run with the default values for the 
primitives hyper parameters. If this pipeline runs successfully a second pipeline
is created with a random selection of hyper parameter values. If this pipeline
runs successfully the primitive is considered successful. If it fails, there will
be 9 further random selections of hyper parameter values attempted before the 
primitive is considered to be non conforming.

To review the most recent primitive performance list visit this page:
https://datadrivendiscovery.gitlab.io/primitives-tests/. Click the pie charts for
 details on what primitives passed and failed.
 
<img src="process_flow.png"
 alt="Primitives Tests Process Flow"
 style="float: left; margin-right: 10px;" />
 
When a primitive is listed as "failed" - this does not necessarily mean that the
primitive is at fault. It could be that the tests are lacking in sophistication 
and need to be improved to handle legitimate use cases. If you discover such a case 
please open an issue in this repo and consider taking on the task.
 
Issues revealed by tests to date (strike throughs are completed):
 
| Primitive        | Team           | Issue  |
| :---------------- |:--------------:| ------:|
| ~~classification.bagging.SKlearn~~ | ~~JPL~~  | ~~https://gitlab.com/datadrivendiscovery/sklearn-wrap/issues/236~~  |
| ~~classification.decision_tree.SKlear~~ | ~~JPL~~  | ~~https://gitlab.com/datadrivendiscovery/sklearn-wrap/issues/237~~ |
| ~~classification.extra_trees.SKlearn~~ | ~~JPL~~  | ~~https://gitlab.com/datadrivendiscovery/sklearn-wrap/issues/238~~ |
| ~~classification.gradient_boosting.SKlearn~~ | ~~JPL~~  | ~~https://gitlab.com/datadrivendiscovery/sklearn-wrap/issues/239~~ |
| ~~classification.k_neighbors.SKlearn~~ | ~~JPL~~  | ~~https://gitlab.com/datadrivendiscovery/sklearn-wrap/issues/240~~ |
| ~~classification.random_forest.SKlearn~~ | ~~JPL~~  | ~~https://gitlab.com/datadrivendiscovery/sklearn-wrap/issues/241~~ |
| classification.light_gbm.Common | Common | https://gitlab.com/datadrivendiscovery/common-primitives/issues/127 |
| classification.gaussian_classification.JHU | JHU | https://gitlab.com/datadrivendiscovery/primitives/issues/361 |
| classification...GeneralRelationalDataset | SRI | https://gitlab.com/datadrivendiscovery/primitives/-/issues/369 |
| ~~regression.bagging.SKlearn~~ | ~~JPL~~  | ~~https://gitlab.com/datadrivendiscovery/sklearn-wrap/issues/233~~ |
| ~~regression.decision_tree.SKlearn~~ | ~~JPL~~  | ~~https://gitlab.com/datadrivendiscovery/sklearn-wrap/issues/234~~ ~~https://gitlab.com/datadrivendiscovery/sklearn-wrap/issues/235~~ |
| ~~regression.extra_trees.SKlearn~~ | ~~JPL~~  | ~~https://gitlab.com/datadrivendiscovery/sklearn-wrap/issues/242~~ |
| ~~regression.k_neighbors.SKlearn~~ | ~~JPL~~  | ~~https://gitlab.com/datadrivendiscovery/sklearn-wrap/issues/243~~ |
| ~~regression.lasso_cv.SKlearn~~ | ~~JPL~~  | ~~https://gitlab.com/datadrivendiscovery/sklearn-wrap/issues/244~~ |
| ~~regression.random_forest.SKlearn~~ | ~~JPL~~  | ~~https://gitlab.com/datadrivendiscovery/sklearn-wrap/issues/245~~ |
| regression.owl_regression.Umich | UMich  | https://gitlab.com/datadrivendiscovery/primitives/-/issues/362 |
| regression...RFMPreconditionedGaussianKRR | ICSI | https://gitlab.com/datadrivendiscovery/primitives/-/issues/363 |
| regression...RFMPreconditionedPolynomialKRR | ICSI | https://gitlab.com/datadrivendiscovery/primitives/-/issues/364 |
| regression...TensorMachinesRegularizedLeastSquares | ICSI | https://gitlab.com/datadrivendiscovery/primitives/-/issues/365 |



# Running Locally

1) Map the directory `seed_datasets_current` to the docker container directory `/data/datasets/seed_datasets_current`. 
(see `primitve-tests/constants.py` to modify this assumption)
2) Run the script `primitives-tests/run-ci.py` from this directory. This 
   will generate a results folder containing:
     * index.html which links a summary of the results and some simple performance  plots together 
     (this is uploaded to `pages` on gitlab after each run)
     * Directory for each primitive type tested which contains:
        * List of failed primitives along with the failure reason (blacklist_file_<type>.txt)
        * List of successful primitives (whitelist_file_<type>.txt)
        * Plots and other information to support the html pages.
        
3) Since the primitives are discovered from whatever docker image is used as the environment,
make sure to work within the most up to date docker image `registry.gitlab.com/datadrivendiscovery/images/core:ubuntu-artful-python36-<version>` 
More information on docker images [here](https://gitlab.com/datadrivendiscovery/images).

# Configuration

The tests are run on the primitives discovered in the docker image specified in the
.gitlab-ci.yml file:

```
   image: registry.gitlab.com/datadrivendiscovery/images/primitives:ubuntu-bionic-python36-stable
```

This should be updated after a release is made to get the latest fixes for the primitives.


# Smoke Test

When changes are made to the code and/or CI system, an environment variable can be used to make the system
run a quick end to end test. When launching a new pipeline on the pipeline page: `https://gitlab.com/datadrivendiscovery/primitives-tests/-/pipelines/new`, 
specify the variable SMOKETEST with value True before clicking Run Pipeline. This will make the system
use only 1 primitive from each primitive family.

When running locally, setting the SMOKETEST environment variable will accomplish the same functionality. 


# Adding new tests

A test consists of a problem type, a pipeline template and a dataset. Currently there are:

    * 2 Problem Types: Regression and Classification
    * 1 Pipeline type: Single Table
    * 2 Datasets: 185_baseball, and 196_autoMpg
    
All of these will be expanded to make the test system more useful. 

## Adding new problem types:
Look at ```tests/classifiction.py``` as an example: It provides the problem type keyword "classification" and the 
preferred dataset to the base class PrimitiveTest which take care of everything else:

    * Finds all the primitives of requested type (classification in this case)
    * Explores the hyper-parameters for each of these primitives and generates ranges of valid values
    * Constructs pipelines with each of the primives and runs them with varied hyper-parameter values
    
## Adding new pipeline templates:
Look at ```templates/single_table_template.py``` as an example. It has 2 methods, _get_prefix and _get_suffix. These
methods build a pipeline in memory for a primitive to be added to. The first method performs all the data wrangling
needed to get the data into the format that the type of primitive (classification, regression etc) will need in
order to operate correctly. The second method takes the output of the target primitive and completes the pipeline.

## Adding new datasets:
All seed datasets are available for the tests to use. To expand the custom datasets that a problem uses, see the 
get_problems method in the test classes (```tests/classifiction.py``` for example). Note that if the problem_shortlist
variable is set to false, all seed_datasets matching the problem type will be used during the test runs. 




    

