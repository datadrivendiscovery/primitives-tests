import d3m
import unittest
import logging
import os
import traceback
import numpy
import json
import random
import matplotlib.pyplot as plt
import operator

from collections import namedtuple
from d3m.exceptions import InvalidArgumentTypeError, InvalidArgumentValueError
from constants import TUNING_PARAMETER, SEED_DATASETS_HOME, OUTPUT_BASE
from grid import hyperparameter_grid
from d3m.runtime import Runtime
from d3m.metadata import base as metadata_base, hyperparams as hyperparams_module, pipeline as pipeline_module, problem
from d3m.container.dataset import Dataset

PrimitiveRecord = namedtuple('PrimitiveRecord', ('blacklisted', 'pclass', 'family', 'hyperparameters', 'performer'))
PrimitiveRecord.__new__.__defaults__ = (False, None, None, None, None)
ProblemRecord = namedtuple('ProblemRecord', ('keywords', 'single_table'))
ProblemRecord.__new__.__defaults__ = defaults=((), False)

# init logger
logging.basicConfig(level=logging.INFO, format='%(asctime)s [%(levelname)s] %(name)s -- %(message)s')
_logger = logging.getLogger(__name__)


# Alternatively, we could make this a subclass of unittest.TestCase
class PrimitiveTestCases:

    class PrimitiveTest(unittest.TestCase):

        def __init__(self, problem_shortlist=True):
            super().__init__()
            self.problem_shortlist = problem_shortlist
            self.all_primitives = {}
            self.all_problems = {}
            self.blacklist = {}
            self.whitelist = {}
            self._load_d3m_primitives()
            self._study_seed_problems()
            self.task_type = ''


        # Abstract method
        def get_task_type(self):
            """
            Returns the name of the task we are working on - this helps separate the output for different test suites
            """
            raise NotImplementedError()


        # Abstract method
        def get_primitives(self):
            """
            Returns an iterable over the primitive classes this test is appropriate for.
            An abstract method that must be implemented by subclasses.
            """
            raise NotImplementedError()


        # Abstract method
        def get_problems(self):
            """
            Returns an iterable over problem identifiers (strings).
            An abstract method that must be implemented by subclasses.
            """
            raise NotImplementedError()


        # Abstract method
        def get_pipeline_template(self):
            """
            Returns a pipeline template (see templatebase module). The assumption is that each primitive we want to test
            will be inserted into a fixed position in the template for testing.
            An abstract method that must be implemented by subclasses.
            """
            raise NotImplementedError()


        def runTest(self):
            """
            Uses the above methods to get a suitable pipeline template and all appropriate problems and primitives.
            Runs the tests involving these items, storing the results for later export.
            """
            # Get the primitives for the specific type of test we are running (tabular regression for example)
            primitives = self.get_primitives()
            # Get the problems that are best suited to test against those primitives
            problems = self.get_problems()
            _logger.info("Testing %s primitives against %s datasets." % (len(primitives), len(problems)))
            # For each of the primitives
            for primitive in primitives:
                # We make 10 attempts to ensure a bad combination of hyper parameters does not disqualify a primitive.
                attempts = 11
                pipeline_succeeded = False
                failure_reason = ""
                for attempt in range(0, attempts):
                    _logger.info("Attempt %s of %s for primitive %s)" % (attempt, attempts, primitive))
                    use_default_hp_values = False
                    if attempt == 0:
                        use_default_hp_values = True
                    status, failure_reason = self.construct_and_run_pipeline(primitive, problems[0],
                                                                      use_default_hp_values=use_default_hp_values)
                    if status is False and attempt is 0:
                        # If the primitive fails with default values we will not continue testing and report failure.
                        _logger.warning("Primitive %s fails with default hyperparameter settings" % primitive)
                        failure_reason = "%s\r\n%s" % (failure_reason,
                                                       "This failure occurred with default hyper parameter values.")
                        break
                    elif status is True and attempt is 0:
                        # The primitive is successful when run with default values for hyper-parameters
                        _logger.info("Primitive %s succeeded with default hyperparameter settings" % primitive)
                        continue
                    elif status is False:
                        _logger.warning("Primitive %s failed attempt %s of %s" % (primitive, attempt, attempts))
                    elif status is True and attempt > 0:
                        _logger.info("Primitive %s passed after %s attempts" % (primitive, attempts))
                        pipeline_succeeded = True
                        break
                    else:
                        _logger.error("Error! This case should never happen - investigate!")

                # If the 10 attempts failed, record the most recent failure - all the others are in the logs
                if pipeline_succeeded is False:
                    self.blacklist[primitive] = failure_reason

            self.export_whitelist()
            self.export_blacklist()
            self.export_stats()

            _logger.info("Generate a config for the compliant %s D3M primitives" % self.get_task_type())
            self.dump_primitive_json_config(self.get_output_path(), "whitelist_config_%s.json" % self.get_task_type())


        def dump_primitive_json_config(self, output_home, whitelist_file):
            """
            Dump any primitive that does not appear in the whitelist. We do this to ensure that the whitelist only contains
            primitives that we are not already loading as part of our basic initialization
            """
            # All available primitives (for this test)
            primitives = self.get_primitives()
            primitives_set = set(primitives)

            _logger.info("Primitives before purge: %s" % len(primitives))

            for primitive in primitives:
                if primitive not in self.whitelist:
                    primitives_set.pop()

            _logger.info("Primitives after purge: %s" % len(primitives))

            # Generate config compatible format for TA2's to exploit the hyperparameter value ranges
            from collections import defaultdict
            primitives_config = defaultdict(lambda: defaultdict(list))
            for primitive in primitives_set:
                hyperparams = self.all_primitives[str(primitive)][3]
                for hyperparam in hyperparams:
                    for value in hyperparams[hyperparam]:
                        primitives_config[str(primitive)][hyperparam].append(value)

            # Write out the updated d3m primitive config file that will be loaded by TA2 at run time
            self.write_config(primitives_config, "%s/%s" % (output_home, whitelist_file))


        def write_config(self, config, config_file):
            """
            Write the white listed primitives and their hyper paramater value ranges to a config file for use by
            ML engines.
            """
            _logger.info("Writing primitive data to config file %s" % config_file)
            with open(config_file, 'w') as file_handle:
                json.dump(config, file_handle, indent=4)


        def construct_and_run_pipeline(self, primitive, problem_name, use_default_hp_values=False):
            # Load a pipeline template suitable for receiving the primitives to be tested
            template = self.get_pipeline_template()
            try:
                # Select a random set of hyperparameter values for the primitive from the range of possible values
                hyperparams = self._generate_random_hyperparams(primitive, use_default_hp_values=use_default_hp_values)
                # Add the primitive to the template with the set of hyperparams
                pipeline = template.instantiate(primitive, hyperparams)
                _logger.info("Successfully generated pipeline for %s " % primitive)
            except Exception as e:
                _logger.warning("Failed to construct pipeline from primitive %s due to: %s" % (primitive, e))
                _logger.warning(traceback.format_exc())
                failure_reason = "%s::\tFailed to construct pipeline.\t Detailed failure: %s %s" % \
                                 (primitive, e, traceback.format_exc())
                return False, failure_reason

            # Convert the format to json for saving
            _logger.info("Attempting to generate pipeline for %s" % (primitive))
            obj = pipeline.to_json_structure()

            output_path = self.get_output_path()

            # Add the pipeline to the logs directory so we can review it if there is a failure
            json_string = json.dumps(obj, indent=4)
            with open("%s/%s.json" % (output_path, primitive), "w") as outfile:
                outfile.write(json_string)

            _logger.info("About to run pipeline for primitive: %s" % (primitive))

            # Prepare the runtime parameters
            path = "file://%s/%s/%s" % (SEED_DATASETS_HOME, problem_name, 'TRAIN/dataset_TRAIN/datasetDoc.json')
            dataset = Dataset.load(dataset_uri=path)
            problem_description = problem.parse_problem_description("%s/%s/%s"
                                                                    % (SEED_DATASETS_HOME, problem_name,
                                                                       'TRAIN/problem_TRAIN/problemDoc.json'))

            # Construct the runtime
            try:
                runtime = Runtime(pipeline=pipeline, problem_description=problem_description,
                                  context=metadata_base.Context.TESTING)
            except Exception as e:
                _logger.warning("Failed to construct runtime for %s, due to %s" % (primitive, e))
                _logger.warning(traceback.format_exc())
                failure_reason = "%s::\tFailed to construct runtime.\t Detailed failure: %s %s" % \
                                 (primitive, e, traceback.format_exc())
                return False, failure_reason

            # Fit the Pipeline
            fit_result = runtime.fit(inputs=[dataset])
            try:
                fit_result.check_success()
            except Exception as e:
                _logger.warning("Failed to fit() %s, due to %s" % (primitive, e))
                _logger.warning(traceback.format_exc())
                failure_reason = "%s::\t Failed to fit().\t Detailed failure: %s %s" % \
                                 (primitive, e, traceback.format_exc())
                return False, failure_reason

            # Produce results using the fitted pipeline
            produce_results = runtime.produce(inputs=[dataset])
            try:
                produce_results.check_success()
            except Exception as e:
                _logger.warning("Failed to produce_results %s, due to %s" % (primitive, e))
                _logger.warning(traceback.format_exc())
                failure_reason = "%s::\t Failed to produce_results().\t Detailed failure: %s %s" % \
                                 (primitive, e, traceback.format_exc())
                return False, failure_reason

            _logger.info("Pipeline succeeded. Predictions: %s" % produce_results.values)
            self.whitelist[primitive] = primitive
            return True, ""

        def get_output_path(self):
            output_path = "%s/%s/" % (OUTPUT_BASE, self.get_task_type())
            if not os.path.exists(output_path):
                os.makedirs(output_path)
            _logger.info("Output path: %s" % output_path)
            _logger.info("Current working directory: %s" % os.getcwd())
            return output_path

        def export_whitelist(self):
            # To be written.  Should raise an exception if tests haven't been run.
            raise NotImplementedError()


        def export_blacklist(self):
            # To be written.  Should raise an exception if tests haven't been run.
            raise NotImplementedError()

        '''
        Generate some summary plots and tables to be presented on the summary page
        '''
        def export_stats(self):
            # Pie chart, where the slices will be ordered and plotted counter-clockwise:
            labels = list()
            pie_slice_types = {}
            total = 0
            for primitive in self.blacklist:
                parts = self.blacklist[primitive].split("\t")
                failure_type = parts[1]
                count = 0
                if failure_type in pie_slice_types:
                    count = pie_slice_types[failure_type]
                count += 1
                pie_slice_types[failure_type] = count
                total += 1
            pie_slice_types["Passed"] = len(self.whitelist)

            pie_sizes = []
            explode = ()
            largest = max(pie_slice_types.items(), key=operator.itemgetter(1))[0]
            for status in pie_slice_types:
                count = pie_slice_types[status]
                labels.append("%s (%s)" % (status, count))
                if total > 0:
                    pie_sizes.append((count / total) * 100)
                else:
                    pie_sizes.append(0)
                # Pop out the largest slice for visual effect
                if status == largest:
                    explode = explode + (0.1,)
                else:
                    explode = explode + (0.0,)

            fig1, ax1 = plt.subplots()
            ax1.pie(pie_sizes, explode=explode, labels=labels, autopct='%1.2f%%',
                    shadow=True, startangle=90)
            ax1.axis('equal')  # Equal aspect ratio ensures that pie is drawn as a circle.

            failed_count = len(self.blacklist)
            passed_count = len(self.whitelist)
            total_count = failed_count + passed_count
            plt.title("%s Primitives Succeeded out of %s Total" %
                      (passed_count, total_count),
                      loc='center')

            plt.savefig("%s/%s%s" % (self.get_output_path(), self.get_task_type(), ".png"))

            # Generate a file with table ready summary data
            summary_template = ""
            with open("primitives-tests/pages/summary.html", "r") as myfile:
                summary_template = myfile.read()

            summary_template = summary_template.replace("FAILED_COUNT", "%s out of %s total" % (failed_count, total_count))
            summary_template = summary_template.replace("PASSED_COUNT", "%s out of %s total" % (passed_count, total_count))

            failed_rows = ""
            for primitive in self.blacklist:
                parts = self.blacklist[primitive].split("\t")
                failure_reason = parts[1]
                failure_details_link = "<a href=\"blacklist_file_%s.txt\">%s</a>" % \
                             (self.get_task_type(), failure_reason)
                performer = self.all_primitives[str(primitive)][4]
                failed_rows += "<tr><td>%s</td><td>%s</td><td>%s</td><td>%s</td></tr>\n" % \
                               (primitive, performer, failure_reason, failure_details_link)

            summary_template = summary_template.replace("FAILED_ROWS", failed_rows)

            passed_rows = ""
            for primitive in self.whitelist:
                performer = self.all_primitives[str(primitive)][4]
                passed_rows += "<tr><td>%s</td><td>%s</td></tr>\n" % (primitive, performer)

            summary_template = summary_template.replace("PASSED_ROWS", passed_rows)

            with open("%s/%s.html" % (self.get_output_path(), self.get_task_type()), "w") as summary_file:
                summary_file.write(summary_template)


        # PRIVATE METHODS

        def _generate_random_hyperparams(self, primitive, use_default_hp_values=False):
            random_hp_values = {}
            possible_values = self.all_primitives[str(primitive)][3]
            for hyperparam in possible_values:
                index = 0
                if use_default_hp_values is True:
                    index = 0
                # Make sure there is more than one value before we go to the effort of randomly picking one
                elif len(possible_values[hyperparam]) > 1:
                    index = random.randint(0, len(possible_values[hyperparam]) - 1)
                random_hp_values[hyperparam] = possible_values[hyperparam][index]
            return random_hp_values


        def _load_d3m_primitives(self):
            primitives = d3m.index.search()
            for primitive in primitives:
                record = self._compute_primitive_info(primitive)
                self.all_primitives[primitive] = record


        def _compute_primitive_info(self, primitive):
            try:
                primitive_obj = d3m.index.get_primitive(primitive)
            except Exception:
                return PrimitiveRecord(blacklisted=True)

            primitive_mdata = primitive_obj.metadata.query()
            family = primitive_mdata['primitive_family']
            performer = primitive_obj.metadata.query()['source']['name']

            _logger.info("Generating HyperParameter Values for primitive: %s" % primitive)
            if primitive == "d3m.primitives.regression.ard.SKlearn":
                _logger.info("yo")
            hp_values_to_try = self._compute_candidate_hp_values(primitive_mdata)
            if hp_values_to_try is None:
                return PrimitiveRecord(blacklisted=True)

            return PrimitiveRecord(pclass=primitive_obj, family=family, hyperparameters=hp_values_to_try, performer=performer)


        def _compute_candidate_hp_values(self, primitive_mdata):

            hp_mdata = primitive_mdata['primitive_code']['class_type_arguments']['Hyperparams']
            hp_defaults = hp_mdata.defaults()
            hyperparams = primitive_mdata['primitive_code']['hyperparams']

            # This will contain hp settings we're willing to consider.
            hp_values_to_try = {}

            for hyperparam in hyperparams:

                if hyperparam not in hp_defaults:
                    continue

                hpclass = hp_mdata.configuration[hyperparam]
                default_value = hyperparams[hyperparam]['default']

                # See if the default value validates, otherwise blacklist.
                # Obviously, HPs that can't validate their default values are problematic.
                try:
                    hpclass.validate(default_value)
                except (InvalidArgumentValueError, InvalidArgumentTypeError):
                    return None

                # if a hyperparameter has a default value of None this does not fail in the validate method
                # if default_value is None:
                #     return None
                values = set()

                if TUNING_PARAMETER in hpclass.semantic_types:
                    try:
                        # Sample some hp values
                        grid_values = hyperparameter_grid(hpclass)
                        if grid_values is None:
                            grid_values = []
                    except Exception as e:
                        # Probably should log the exception here and debug it
                        grid_values = []

                    # Add any grid values that validate
                    for value in grid_values:
                        try:
                            hpclass.validate(value)
                            values.add(value)
                        except (InvalidArgumentTypeError, InvalidArgumentValueError):
                            pass

                # This ensures the first value in the list of possible values is the default. We use this to ensure we
                # can construct a pipeline with default hyper-parameters as a first test before trying random values.
                values_list = list(values)
                values_list.insert(0, default_value)
                hp_values_to_try[hyperparam] = values_list

            return hp_values_to_try


        def _study_seed_problems(self):
            # Queries available seed problems and notes the information we need to select datasets later.
            self.all_problems = self.get_problem_list(SEED_DATASETS_HOME)

        # CONVENIENCE METHODS BELOW

        def get_primitives_in_families(self, *families):
            # The smoke test makes the system run tests on only 1 primitive from each family to confirm CI is running
            # as expected.
            if os.getenv('SMOKETEST') is not None:
                logging.info("Running Smoke Test, returning 1 primitive")
                return [[ p.pclass for p in self.all_primitives.values() if (not p.blacklisted) and (p.family in families) ][0]]
            else:
                logging.info("Running full suite of tests")
                return [ p.pclass for p in self.all_primitives.values() if (not p.blacklisted) and (p.family in families) ]


        def get_problems_of_type(self, *type_keywords, single_table=False):
            type_matchers = [ name for name, record in self.all_problems.items()
                              if all(kw in record.keywords for kw in type_keywords) ]
            if single_table:
                type_matchers = [ name for name in type_matchers if self.all_problems[name].single_table ]
            return type_matchers


        def get_problem_list(self, dataset_home):
            # Returns a list of problem names from the seed data set directory indicated in the environment variable
            problem_list = []

            exists = os.path.isdir(dataset_home)
            _logger.info("Dataset directory (%s) exists? %s" % (dataset_home, exists))
            readable = os.access(dataset_home, os.R_OK)
            _logger.info("Dataset directory (%s) readable? %s" % (dataset_home, readable))

            directories_list = next(os.walk(dataset_home))[1]
            for directory in directories_list:
                # Ensure this is an actual problem dir
                sub_directories_list = next(os.walk("%s/%s" % (dataset_home, directory)))[1]
                for sub_directory in sub_directories_list:
                    if sub_directory == "SCORE":
                        # This is a legit problem dir
                        problem_list.append(directory)
                        break
            return problem_list
