from d3m.metadata.base import ArgumentType
from d3m.metadata.pipeline import Pipeline, PrimitiveStep


# ABC that specifies the template API
class PipelineTemplate(object):

    def instantiate(self, primitive_class, hyperparams):
        """
        Accepts the class of a D3M primitive and produces a pipeline suitable for testing it.
        """
        pipeline, node, target_node = self._get_prefix()
        # We need to think through how hyperparameters are set
        primitive = self._instantiate_primitive(primitive_class, hyperparams=hyperparams)
        node = self._append_primitive(pipeline, primitive, node, outputs=target_node)
        output = self._append_suffix(pipeline, node, target_node)

        # Add the output handle to the pipeline
        pipeline.add_output(data_reference=output)

        return pipeline

    # Below are just some ideas for how to make it easy to implement subclasses

    def _get_prefix(self):
        """
        Returns three things:
            A pipeline.
            A reference to the output node of the last step.
            A reference to the 'target' node.
        An abstract method that must be implemented by subclasses.
        """
        raise NotImplementedError()

    def _append_suffix(self, pipeline, node, target_node):
        """
        Returns a full pipeline, by appending any needed primitives to the end of the pipeline.
        :param node:
        :param target_node:
        """
        # A no-op by default.  Alternatively, this method might be used to add ConstructPredictions
        # if that always needs to be present.
        return

    # This primitive could do more.  For example, it could choose a random configuration of the primitive's
    # tuning parameters.  Not sure whether this is the right place to do that.
    def _instantiate_primitive(self, pclass, hyperparams):
        return pclass(hyperparams=hyperparams)

    def _append_primitive(self, pipeline, primitive, node, **other_inputs):
        pclass = type(primitive)
        mdata = pclass.metadata.query()
        required_args = mdata['primitive_code'].get('arguments', {})
        pstep = PrimitiveStep(primitive_description=mdata)
        pstep.add_argument("inputs", ArgumentType.CONTAINER, node)
        for arg, onode in other_inputs.items():
            if required_args.get(arg, None) is not None:
                pstep.add_argument(arg, ArgumentType.CONTAINER, onode)
        pstep.add_output("produce")
        for k, v in primitive.hyperparams.items():
            pstep.add_hyperparameter(name=k, argument_type=ArgumentType.VALUE, data=v)
        pipeline.add_step(pstep)
        return "steps.%d.produce" % (len(pipeline.steps) - 1)

    def _empty_pipeline(self):
        return Pipeline()





