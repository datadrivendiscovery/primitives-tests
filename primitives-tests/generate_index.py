from os import walk
import datetime

'''
Called after the tests have run. This collects up the plots and stats and generates a landing page for presenting the
CI results
'''

'''
Find the pie chart plots for addition to the summary page
'''
def find_files(dir_name, extension):
    plot_files = []
    for root, dirs, files in walk(dir_name):
        for name in files:
            if name.endswith(extension):
                plot_files.append("%s/%s" % (root.replace(dir_name, ""), name))
    return plot_files


'''
Entry point - required to make python happy
'''
if __name__ == "__main__":
    # Read configuration for summary header
    docker_version = ""
    with open('.gitlab-ci.yml') as run_config:
        docker_version = run_config.readline()
    docker_version = docker_version.replace("image: ", "")

    run_date = datetime.datetime.now().strftime("%I:%M%p on %B %d, %Y")

    header = "<head>\n</head>\n" \
             "<body>\n" \
             "<h1 style=\"color: black; text-align:center\">Primitive Tests Results</h1>\n" \
             "<h4 style=\"color: blue; text-align:center\">Docker Version: %s</h4>\n" \
             "<h4 style=\"color: blue; text-align:center\">Run Date: %s</h4>\n" \
             "<hr>\n" % (docker_version, run_date)
    tail = "</body>"

    # Gather up the plots
    plots = find_files("results/", "png")

    # For each type of test generate a page and link it to the main one
    index_file = open("results/index.html", "w")
    index_file.write(header)
    for plot in plots:
        parts = plot.split("/")
        task_type = parts[0]
        index_file.write("<h2 style=\"margin-left: +100px;color:blue\">%s:</h2>"
                         "<a href=\"%s.html\"><img style=\"margin-left: +100px;\"src=\"%s.png\"></a>" %
                         (task_type.capitalize(), task_type, task_type))

    # Finalize the index
    index_file.write(tail)
    index_file.close()