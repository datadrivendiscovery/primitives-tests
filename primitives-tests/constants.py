VERSION = '20200219'
SEED_DATASETS_HOME = '/data/datasets/seed_datasets_current'
OUTPUT_BASE = 'results'

TUNING_PARAMETER = 'https://metadata.datadrivendiscovery.org/types/TuningParameter'

ATTRIBUTE_TYPE = "https://metadata.datadrivendiscovery.org/types/Attribute"
TRUE_TARGET_TYPE = "https://metadata.datadrivendiscovery.org/types/TrueTarget"
ENTRY_POINT_TYPE = 'https://metadata.datadrivendiscovery.org/types/DatasetEntryPoint'
SUGGESTED_TARGET_TYPE = "https://metadata.datadrivendiscovery.org/types/SuggestedTarget"
TARGET_TYPE = "https://metadata.datadrivendiscovery.org/types/Target"
PRIMARY_KEY_TYPE = 'https://metadata.datadrivendiscovery.org/types/PrimaryKey'
GROUPING_KEY_TYPE = 'https://metadata.datadrivendiscovery.org/types/GroupingKey'
UNIQUE_KEY_TYPE = 'https://metadata.datadrivendiscovery.org/types/UniqueKey'
PRIMARY_MULTI_KEY_TYPE = 'https://metadata.datadrivendiscovery.org/types/PrimaryMultiKey'
SUGGESTED_GROUPING_KEY_TYPE = 'https://metadata.datadrivendiscovery.org/types/SuggestedGroupingKey'

BOOLEAN_TYPE = 'http://schema.org/Boolean'
CATEGORICAL_TYPE = 'https://metadata.datadrivendiscovery.org/types/CategoricalData'
INTEGER_TYPE = 'http://schema.org/Integer'
FLOAT_TYPE = 'http://schema.org/Float'
FLOAT_VECTOR_TYPE = 'https://metadata.datadrivendiscovery.org/types/FloatVector'
DATETIME_TYPE = 'http://schema.org/DateTime'

KEY_TYPES = [PRIMARY_KEY_TYPE, GROUPING_KEY_TYPE, UNIQUE_KEY_TYPE, PRIMARY_MULTI_KEY_TYPE, SUGGESTED_GROUPING_KEY_TYPE]
SCALAR_TYPES = [BOOLEAN_TYPE, CATEGORICAL_TYPE, INTEGER_TYPE, FLOAT_TYPE, DATETIME_TYPE]
