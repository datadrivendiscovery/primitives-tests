import unittest

import os
import sys
import logging
import traceback
CURRENT_DIR = os.path.dirname(os.path.abspath(__file__))
sys.path.append(os.path.dirname(CURRENT_DIR))
sys.path.append(os.path.abspath('../'))

from testbase import PrimitiveTestCases
from templates.single_table_template import SingleTableTemplate

# init logger
logging.basicConfig(level=logging.INFO, format='%(asctime)s [%(levelname)s] %(name)s -- %(message)s')
_logger = logging.getLogger(__name__)


class ClassificationTest(PrimitiveTestCases.PrimitiveTest):

    def get_primitives(self):
        return self.get_primitives_in_families('CLASSIFICATION')


    def get_task_type(self):
        return 'classification'


    def get_problems(self):
        if self.problem_shortlist:
            return ['185_baseball_MIN_METADATA']
        else:
            return self.get_problems_of_type('classification', single_table=True)


    def get_pipeline_template(self):
        return SingleTableTemplate()


    def export_blacklist(self):
        blacklist_file_handle = open("%s/%s" % (self.get_output_path(), "blacklist_file_classification.txt"), "w+")
        blacklist_file_handle.write("%s\t%s\t%s\n\n" % ("Blacklisted Primitives", "Failure Message", "Detailed Failure Message"))
        for primitive in self.blacklist:
            blacklist_file_handle.write("%s" % self.blacklist[primitive])
            blacklist_file_handle.write("\n================================================\n\n")
            blacklist_file_handle.flush()
        blacklist_file_handle.close()


    def export_whitelist(self):
        whitelist_file_handle = open("%s/%s" % (self.get_output_path(), "whitelist_file_classification.txt"), "w+")
        whitelist_file_handle.write("%s\n" % "Whitelisted Primitives")
        for primitive in self.whitelist:
            whitelist_file_handle.write("%s\n" % self.whitelist[primitive])
            whitelist_file_handle.flush()
        whitelist_file_handle.close()

if __name__ == '__main__':
    unittest.main()