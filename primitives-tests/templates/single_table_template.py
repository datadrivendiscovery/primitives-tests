from d3m.primitives.data_preprocessing.dataset_text_reader import DatasetTextReader
from d3m.primitives.data_transformation.dataset_to_dataframe import Common as DatasetToDataFrame
from d3m.primitives.schema_discovery.profiler import Common as SimpleProfiler
from d3m.primitives.data_transformation.column_parser import Common as ColumnParser
from d3m.primitives.data_transformation.simple_column_parser import DataFrameCommon as SimpleColumnParser
from d3m.primitives.data_transformation.extract_columns_by_semantic_types import Common as ExtractColumnsBySemanticTypes
from d3m.primitives.data_transformation.conditioner import Conditioner
from d3m.primitives.data_transformation.construct_predictions import Common as ConstructPredictions

from constants import ATTRIBUTE_TYPE, TRUE_TARGET_TYPE, SCALAR_TYPES
from templatebase import PipelineTemplate

class SingleTableTemplate(PipelineTemplate):

    def _get_prefix(self):
        """
        Returns three things:
            A pipeline.
            A reference to the output node of the last step.
            A reference to the 'target' node.
        """
        tr = DatasetTextReader(hyperparams={})
        todf = DatasetToDataFrame(hyperparams=dict(dataframe_resource=None))
        simple_profiler = SimpleProfiler(hyperparams={})
        scp = SimpleColumnParser(hyperparams={})
        cp = ColumnParser(hyperparams=dict(parse_semantic_types=tuple(SCALAR_TYPES)))
        ext_attr = ExtractColumnsBySemanticTypes(hyperparams=dict(semantic_types=(ATTRIBUTE_TYPE,)))
        cond = Conditioner(hyperparams=dict(ensure_numeric=True, maximum_expansion=30))
        ext_targ = ExtractColumnsBySemanticTypes(hyperparams=dict(semantic_types=(TRUE_TARGET_TYPE,)))
        
        pipeline = self._empty_pipeline()
        pipeline.add_input('inputs.0')
        node = 'inputs.0'

        # Get a dataframe
        node = self._append_primitive(pipeline, tr, node)

        node = self._append_primitive(pipeline, todf, node)

        # Add primitive to add the metadata back in
        node = self._append_primitive(pipeline, simple_profiler, node)

        # Extract target
        tnode = self._append_primitive(pipeline, ext_targ, node)
        tnode = self._append_primitive(pipeline, scp, tnode)

        # Extract features
        node = self._append_primitive(pipeline, cp, node)
        node = self._append_primitive(pipeline, ext_attr, node)
        node = self._append_primitive(pipeline, cond, node)

        return pipeline, node, tnode

    def _append_suffix(self, pipeline, node, target_node):
        """
        Returns a full pipeline, by appending any needed primitives to the end of the pipeline.
        :param node:
        :param target_node:
        """
        # Find the Dataset To DataFrame step and use that as the input to the last step: ConstructPredictions. This
        # is required as the ConstructPredictions primitive needs the original input data with the d3mIndex column
        reference_step = None
        for idx, step in enumerate(pipeline.steps):
            if step.primitive.__name__ == 'DatasetToDataFramePrimitive':
                reference_step = 'steps.%s.produce' % idx

        # Add in the ConstructPredictions primitive as the last step - this will format the predictions appropriately
        format_predictions = ConstructPredictions(hyperparams={})

        # The ConstructPredictions primitive will need a reference=DataFrame during produce
        output = self._append_primitive(pipeline, format_predictions, node,
                                     reference=reference_step, outputs=target_node)

        return output

